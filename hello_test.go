package main

import "testing"

func TestHello(t *testing.T) {
	t.Run("Is Hello World Working?", func(t *testing.T) {
		got := Hello()
		want := "Hello, World"

		if got != want {
			t.Errorf("got '%s', while I wanted '%s'", got, want)
		}
	})

}
